const redis = require('redis');
const {promisify} = require('util');
const {log} = require('../../../log');

let client = null;
let keyWait = null;
const PROCESS_WAIT = 100;
const BLPOP_WAIT = 1;

let blpop = null;
let lpush = null;
let keepDoing = true;
let timeout = null;

function readRequest(options) {
  if (options.idRequest) {
    return blpop(options.askRequest, 1)
    .then((reply) => {
      if (reply) {
        const [, str] = reply;

        options.data = JSON.parse(str);
      }
      return options;
    });
  }
  return options;
}

// eslint-disable-next-line no-ternary
const sendResponse = (key, value) => lpush(key, typeof value === 'string'
                                                  ? value
                                                  : JSON.stringify(value));

// eslint-disable-next-line max-statements
function doRequest(options) {
  if (options.data) {
    log.trace('doRequest', options.data);
    const [action] = options.idRequest.split('_');

    let response = null;

    if (action === 'BADJSON') {
      response = '{"success": true, "params": {"beta":, "alpha": 1}}';
    } else {
      response = {
        success: true,
        result: options.data
      };
    }
    return sendResponse(options.ansRequest, response)
    .then((reply) => {
      log.info('[WORKER CLIENT] -- done', options.ansRequest, reply);
    });
  }
  if (options.ansRequest) {
    return sendResponse(options.ansRequest, {
      success: false,
      error: {
        message: 'no data provided'
      }
    });
  }
  return Promise.resolve(1);
}

function waitRequest() {
  blpop(keyWait, BLPOP_WAIT)
  .then((reply) => {
    if (reply) {
      log.info('[WORKER CLIENT] -- requested', reply);
      return {
        idRequest: reply[1],
        askRequest: `ASK_${reply[1]}`,
        ansRequest: `ANS_${reply[1]}`
      };
    }
    return {idRequest: null};
  })
  .then(readRequest)
  .then(doRequest)
  .catch((reason) => {
    log.info('[WORKER CLIENT] -- error', reason);
  })
  .then(() => {
    if (keepDoing) {
      clearTimeout(timeout);
      timeout = setTimeout(waitRequest, PROCESS_WAIT);
    }
  });
}

function connect(key) {
  client = redis.createClient();

  blpop = promisify(client.blpop).bind(client);
  lpush = promisify(client.lpush).bind(client);
  client.on('connect', () => {
    keyWait = key;
    log.info(`[WORKER CLIENT] -- connected to ${keyWait}`);
    timeout = setTimeout(waitRequest, PROCESS_WAIT);
  });

  return 1;
}

function disconnect() {
  keepDoing = false;
  clearTimeout(timeout);
  client.quit();
  return 1;
}

exports.connect = connect;
exports.disconnect = disconnect;

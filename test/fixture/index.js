const request = require('request-promise-native');
const jsonrpcHelper = require('jsonrpc-lite');

exports.preflightRequest = (url, method, route) => {
  const options = {
    method: 'OPTIONS',
    uri: `${url}${route}`,
    headers: {
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
      'Accept-Language': 'en-US,en;q=0.5',
      'Accept-Encoding': 'gzip, deflate',
      'Access-Control-Request-Method': method,
      'Access-Control-Request-Headers': 'authorization',
      'Connection': 'keep-alive',
      'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0'
    },
    resolveWithFullResponse: true,
    simple: false
  };

  return request(options);
};

exports.post = (url, rpc, params) => {
  let body = {};

  // eslint-disable-next-line no-negated-condition
  if (!rpc) {
    // eslint-disable-next-line no-undefined
    body = undefined;
  } else {
    const id = 0;

    body = jsonrpcHelper.request(id, rpc, params);
   }

  const options = {
    method: 'POST',
    uri: url,
    body,
    // Automatically stringifies the body to JSON
    json: true
  };

  return request(options);
};


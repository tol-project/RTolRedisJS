/* global describe, before, after, it */

// eslint-disable-next-line no-process-env
process.env.NODE_ENV = 'test';
// eslint-disable-next-line no-process-env
process.env.DEBUG = 'app:warning';

const fixture = require('./fixture');
const config = require('config');
const fakeServer = require('./fixture/fake-worker/worker');
const chai = require('chai');
const expressListRoutes = require('express-list-routes');
const jsonrpcHelper = require('jsonrpc-lite');
const {log} = require('../log');

chai.use(require('chai-as-promised'));
chai.use(require('chai-fs'));
chai.should();

const {expect} = chai;
const decache = require('decache');

const HTTP204 = 204;

// eslint-disable-next-line no-unused-vars
function dumpResult(result) {
  log.trace(JSON.stringify(result, null, '  '));
  return result;
}

function serviceStart() {
  decache('../service');
  // eslint-disable-next-line global-require
  const service = require('../service');

  // eslint-disable-next-line arrow-body-style
  return service.start().then((ctx) => {
    // console.log(`Main service started on port=${ctx.server.address().port}`);
    return {
      service,
      url: `http://localhost:${ctx.server.address().port}${ctx.routerPath}`,
      ctx
    };
  });
}

function serviceClose(context) {
  return context.service.close()
  .then(() => {
    decache('../service');
  });
}

function preflightRequest(method, route) {
  return serviceStart()
  .then((context) => fixture.preflightRequest(context.url, method, route)
  .then((response) => serviceClose(context).then(() => response)));
}

function doPost(method, body) {
  return serviceStart()
  .then((context) => fixture.post(context.url, method, body)
  .then((response) => serviceClose(context).then(() => response)));
}

function listAPI(context) {
  expressListRoutes({}, 'API:', context.ctx.routerAPI);
  return context;
}

function buildError(rpcErrorObject) {
  const errorExpected = jsonrpcHelper.error(0, rpcErrorObject);

  return JSON.parse(JSON.stringify(errorExpected));
}

function buildSuccess(result) {
  return jsonrpcHelper.success(0, result);
}

describe('RTOL_REDIS_JS', () => {
  before(() => fakeServer.connect(config.get('taskQueue')));

  describe('smoke test & branch coverage', () => {
    it('should start & close smoothly',
    () => serviceStart()
    .then(listAPI)
    .then(serviceClose));
    it('preflight POST', () => {
      const result = preflightRequest('POST', '')
      // .then(dumpResult)
      .then((noDate) => {
        Reflect.deleteProperty(noDate.headers, 'date');
        return noDate;
      });

      return expect(result).to.eventually.deep.contain({
        statusCode: HTTP204,
        body: '',
        headers: {
          'x-powered-by': 'Express',
          'access-control-allow-origin': '*',
          'access-control-allow-methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
          'vary': 'Access-Control-Request-Headers',
          'access-control-allow-headers': 'authorization',
          'content-length': '0',
          'connection': 'keep-alive'
        }
      });
    });
  });

  describe('invalid requests', () => {
    it('it return 200 & invalid request on empty body', () => {
      const result = doPost();

      return expect(result).to.eventually.deep.contain({
        jsonrpc: '2.0',
        id: 0,
        error: {
          message: 'Invalid request',
          code: -32600,
          data: {
            message: 'body is not a valid JSON-RPC request or notification',
            body: {}
          }
        }
      });
    });

    it('it return 200 & invalid request on unknown RPC method', () => {
      const result = doPost('method_not_found');

      return expect(result).to.eventually.deep.contain({
        jsonrpc: '2.0',
        id: 0,
        error: {
          message: 'Method not found',
          code: -32601,
          data: {
            'method': 'method_not_found',
            'endpoint': '/jsonrpc/rredis/v0'
          }
        }
      });
    });

    it('it return 200 & invalid request on empty params', () => {
      const result = doPost('callAction');
      const errorExpected = buildError(
        jsonrpcHelper.JsonRpcError.invalidParams({
          message: 'missing parameters',
          parameter: 'params'
        })
      );

      return expect(result).to.eventually.deep.contain(errorExpected);
    });

    it('it return 200 & invalid request on empty params.action', () => {
      const result = doPost('callAction', {});
      const errorExpected = buildError(
        jsonrpcHelper.JsonRpcError.invalidParams({
          message: 'missing parameters',
          parameter: 'params.action'
        })
      );

      return expect(result).to.eventually.deep.contain(errorExpected);
    });

    it('it return 200 & invalid request on empty params.data', () => {
      const result = doPost('callAction', {action: 'AIA'});
      const errorExpected = buildError(
        jsonrpcHelper.JsonRpcError.invalidParams({
          message: 'missing parameters',
          parameter: 'params.data'
        })
      );

      return expect(result).to.eventually.deep.contain(errorExpected);
    });

    it('it return JSON-RPC error on invalid worker response ', () => {
      const data = {hello: 'world'};
      const result = doPost('callAction', {
        action: 'BADJSON',
        data
      }).then(dumpResult);

      const REDIS_WERROR = -40001;
      const dataError = {
        name: 'SyntaxError',
        context: 'parsing worker response',
        description: 'Unexpected token , in JSON at position 36',
        workerResponse: '{"success": true, "params": {"beta":, "alpha": 1}}'
      };

      const errorExpected = buildError(new jsonrpcHelper.JsonRpcError('redis worker error', REDIS_WERROR, dataError));

      return expect(result).to.eventually.deep.contain(errorExpected);
    });

  });

  describe('valid requests', () => {
    it('returns cancelled if WRONG_QUEUE', () => {
      const data = {hello: 'world'};
      const result = doPost('callAction', {
        taskQueue: 'WRONG_QUEUE',
        action: 'AIA',
        data
      });

      const ERROR_CANCEL = -40000;
      const errorExpected = buildError(
        new jsonrpcHelper.JsonRpcError('redis response cancelled', ERROR_CANCEL, {})
      );

      return expect(result).to.eventually.deep.contain(errorExpected);
    });

    it('returns valid response on explicit queue', () => {
      const data = {hello: 'world'};
      const result = doPost('callAction', {
        taskQueue: 'QUEUE_DEFAULT',
        action: 'AIA',
        data
      }).then(dumpResult);

      const successExpected = buildSuccess(data);

      return expect(result).to.eventually.deep.contain(successExpected);
    });

    it('returns valid response on default queue', () => {
      const data = {hello: 'world'};
      const result = doPost('callAction', {
        action: 'AIA',
        data
      }).then(dumpResult);

      const successExpected = buildSuccess(data);

      return expect(result).to.eventually.deep.contain(successExpected);
    });
  });

  after(() => fakeServer.disconnect());

});

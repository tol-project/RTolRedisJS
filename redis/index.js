const {promisify} = require('util');
const redis = require('redis');
const {log} = require('../log');

let client = null;

function quit() {
  client.quit();
  return Promise.resolve(1);
}

// eslint-disable-next-line max-statements
function connect(host, port) {
  log.info(`host: ${host}`);
  client = redis.createClient({
    host,
    port
  });
  exports.set = promisify(client.set).bind(client);
  exports.get = promisify(client.get).bind(client);
  exports.lpush = promisify(client.lpush).bind(client);
  exports.blpop = promisify(client.blpop).bind(client);
  exports.duplicate = promisify(client.duplicate).bind(client);

  client.on('connect', () => {
    log.info(`[REDIS CLIENT] -- connected to ${client.address}`);
  });

  client.on('error', (err) => {
    log.error(`[REDIS CLIENT] -- error ${err}`);
  });

  return Promise.resolve(1);
}

exports.connect = connect;
exports.quit = quit;

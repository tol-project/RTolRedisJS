const jsonrpcHelper = require('jsonrpc-lite');
const redis = require('../redis');
const strftime = require('strftime');
const uuid1 = require('uuid/v1');
const config = require('config');
const {JsonRpcError} = require('jsonrpc-lite');
const {log} = require('../log');

function invalidParams(param) {
  let tail = '';

  if (param) {
    tail = `.${param}`;
  }
  throw jsonrpcHelper.JsonRpcError.invalidParams({
    'message': 'missing parameters',
    'parameter': `params${tail}`
  });
}

function makeId(options) {
  let compressor = 'none';
  const time = strftime('%Y%m%d_%H%M%S');
  const uid = uuid1();

  if (options.compressor) {
    ({compressor} = options);
  }

  // eslint-disable-next-line max-len
  const idRequest = `${options.action}_${compressor}_${time}_${options.user}_${options.project}_${uid}`;

  return idRequest;
}

function pushRequest(options) {
  const idRequest = makeId(options);
  const askRequest = `ASK_${idRequest}`;
  const ansRequest = `ANS_${idRequest}`;

  log.info('[pushRequest] -- ', options.taskQueue, idRequest);

  return redis.lpush(askRequest, JSON.stringify(options.data))
  .then(() => redis.lpush(options.taskQueue, idRequest))
  .then(() => ({
    idRequest,
    askRequest,
    ansRequest
  }));
}

function popResponse (request) {
  log.info('[popResponse] -- ', request.ansRequest);
  return redis.duplicate()
  .then((client) =>
    new Promise((resolve, reject) => {
      const timeout = setTimeout(() => {
        log.info('blocking redis client response cancelled', request.ansRequest);
        client.end(false);

        const REDIS_CANCELLED = -40000;

        reject(new JsonRpcError('redis response cancelled', REDIS_CANCELLED, {}));
      }, config.get('timeoutBlpop'));

      // eslint-disable-next-line max-statements
      client.blpop(request.ansRequest, 0, (err, reply) => {
        clearTimeout(timeout);
        client.end(false);
        if (err) {
          reject(err);
        } else {
          log.debug(reply[1]);
          // JSON.parse(reply[1]);
          log.trace('popResponse is going to parse', reply[1]);

          let redisResponse = null;

          try {
            redisResponse = JSON.parse(reply[1]);
          } catch (error) {
            redisResponse = {
              success: false,
              error: {
                name: error.name,
                context: 'parsing worker response',
                description: error.message,
                workerResponse: reply[1]
              }
            };
          }
          if (redisResponse.success) {
            resolve(redisResponse.result);
          } else {
            const REDIS_WERROR = -40001;

            reject(new JsonRpcError('redis worker error', REDIS_WERROR, redisResponse.error));
          }
        }
      });
    }));
}

// eslint-disable-next-line max-statements
function callAction(req) {
  const {params} = req.body;

  if (!params) {
    throw invalidParams('');
  }

  const {action} = params;

  if (!action) {
    throw invalidParams('action');
  }

  const {data} = params;

  if (!data) {
    throw invalidParams('data');
  }

  let {taskQueue} = params;

  if (!taskQueue) {
    taskQueue = config.get('taskQueue');
  }

  return pushRequest({
    taskQueue,
    action,
    compressor: 'none',
    data,
    project: 'project',
    user: 'user'
  })
  .then(popResponse);
}

exports.callAction = callAction;

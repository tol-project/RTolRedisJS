const service = require('./service');
const listAPI = require('express-list-routes');
const config = require('config');

const {log} = require('./log');

// eslint-disable-next-line no-magic-numbers
log.info(JSON.stringify(config, null, 2));

service.start()
.then((ctx) => {
  listAPI({}, ctx.routerAPI);
  log.info(`Server listening on port ${ctx.server.address().port}`);
})
.catch((reason) => {
  log.eror(reason);
});

const config = require('config');
const express = require('express');
const jsonrpc = require('jsonrpc2-express');
const cors = require('cors');
const morgan = require('morgan');
const jsonrpcMethods = require('./jsonrpc');
const redis = require('./redis');

const app = express();

app.options('*', cors());
app.use(cors());

app.use(morgan('common', {
  skip: () => config.get('env') === 'test'
}));

const routerAPI = new express.Router();
const routerPath = `${config.get('urlPrefix')}/jsonrpc/rredis/v0`;

jsonrpc(routerPath, routerAPI, {
  methods: jsonrpcMethods,
  bodyParser: {
    limit: '50mb'
  }
});

app.use('/', routerAPI);

let server = null;

function startServer() {
  return new Promise((resolve) => {
    server = app.listen(config.get('port'), () => {
      resolve({
        server,
        routerAPI,
        routerPath
      });
    });
  });
}

function start() {
  return redis.connect(config.get('redisHost'), config.get('redisPort'))
  .then(startServer);
}

function close() {
  return Promise.resolve(server.close()).then(redis.quit);
}

exports.start = start;
exports.close = close;
exports.app = app;
